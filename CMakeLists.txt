cmake_minimum_required(VERSION 3.23)
project(linux_application_development_homework C)

set(CMAKE_C_STANDARD 11)

add_executable(linux_application_development_homework
        01_GitBuildReq/hello_world.c)
